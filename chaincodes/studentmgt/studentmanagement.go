package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Student struct {
	ID               string    `json:"id"`
	Name             string    `json:"name"`
	DateOfBirth      time.Time `json:"dateOfBirth"`
	Gender           string    `json:"gender"`
	GraduationStatus bool      `json:"graduationStatus"`
}

// RecordCount struct keeps track of the number of records
type RecordCount struct {
	Count int `json:"count"`
}

type StudentContract struct {
	contractapi.Contract
}

func (s *StudentContract) CreateStudent(ctx contractapi.TransactionContextInterface, id string, name string,
	dateOfBirth time.Time, gender string, graduationStatus bool) error {
	// Check if a student record with the given id already exists
	existingStudentJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return err
	}

	if existingStudentJSON != nil {
		return fmt.Errorf("a student record with the id '%s' already exists", id)
	}

	student := Student{
		ID:               id,
		Name:             name,
		DateOfBirth:      dateOfBirth,
		Gender:           gender,
		GraduationStatus: graduationStatus,
	}
	studentJSON, err := json.Marshal(student)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, studentJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Created student: %s", id)
	err = ctx.GetStub().SetEvent("CreateStudent", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	// Update record count
	return s.UpdateRecordCount(ctx, 1)
}

func (s *StudentContract) ReadStudent(ctx contractapi.TransactionContextInterface, id string) (*Student, error) {
	studentJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if studentJSON == nil {
		return nil, fmt.Errorf("the student %s does not exist", id)
	}
	var student Student
	err = json.Unmarshal(studentJSON, &student)
	if err != nil {
		return nil, err
	}
	eventPayload := fmt.Sprintf("Read student: %s", id)
	err = ctx.GetStub().SetEvent("ReadStudent", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register. %v", err)
	}
	return &student, nil
}
func (s *StudentContract) UpdateStudent(ctx contractapi.TransactionContextInterface, id string, name string,
	dateOfBirth time.Time, gender string, graduationStatus bool) error {
	student, err := s.ReadStudent(ctx, id)
	if err != nil {
		return err
	}
	student.Name = name
	student.DateOfBirth = dateOfBirth
	student.Gender = gender
	student.GraduationStatus = graduationStatus
	studentJSON, err := json.Marshal(student)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, studentJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Updated student: %s", id)
	err = ctx.GetStub().SetEvent("UpdateStudent", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	// Update record count
	return s.UpdateRecordCount(ctx, -1)

}

// Record Count
// UpdateRecordCount updates the total number of student records
func (s *StudentContract) UpdateRecordCount(ctx contractapi.TransactionContextInterface, increment int) error {
	countJSON, err := ctx.GetStub().GetState("recordCount")
	if err != nil {
		return err
	}
	var recordCount RecordCount
	if countJSON == nil {
		recordCount = RecordCount{Count: 0}
	} else {
		err = json.Unmarshal(countJSON, &recordCount)
		if err != nil {
			return err
		}
	}
	recordCount.Count += increment
	newCountJSON, err := json.Marshal(recordCount)
	if err != nil {
		return err
	}
	return ctx.GetStub().PutState("recordCount", newCountJSON)
}

// GetRecordCount returns the total number of student records
func (s *StudentContract) GetRecordCount(ctx contractapi.TransactionContextInterface) (int, error) {
	countJSON, err := ctx.GetStub().GetState("recordCount")
	if err != nil {
		return 0, err

	}
	var recordCount RecordCount
	err = json.Unmarshal(countJSON, &recordCount)
	if err != nil {
		return 0, err
	}
	return recordCount.Count, nil
}

func (s *StudentContract) DeleteStudent(ctx contractapi.TransactionContextInterface, id string) error {
	err := ctx.GetStub().DelState(id)
	if err != nil {
		return fmt.Errorf("failed to delete state: %v", err)
	}
	eventPayload := fmt.Sprintf("Deleted student: %s", id)
	err = ctx.GetStub().SetEvent("DeleteStudent", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

//Functionality: Implement a function to retrieve the history of changes made to a student record.
func (s *StudentContract) GetStudentHistory(ctx contractapi.TransactionContextInterface, id string) ([]*Student, error) {
	resultsIterator, err := ctx.GetStub().GetHistoryForKey(id)
	if err != nil {
		fmt.Printf("Error retrieving history for key %s: %v\n", id, err)
		return nil, err
	}
	defer resultsIterator.Close()

	var history []*Student
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			fmt.Printf("Error iterating history for key %s: %v\n", id, err)
			return nil, err
		}

		var student Student
		if len(response.Value) > 0 { // Check if there is a value
			err = json.Unmarshal(response.Value, &student)
			if err != nil {
				fmt.Printf("Error unmarshalling student history record: %v\n", err)
				return nil, err
			}
		}
		history = append(history, &student)
	}
	fmt.Printf("Retrieved history for key %s: %v\n", id, history)
	return history, nil
}

//Functionality: Implement a function specifically for updating the graduation status of a student.
func (s *StudentContract) UpdateGraduationStatus(ctx contractapi.TransactionContextInterface, id string, graduationStatus bool) error {
	student, err := s.ReadStudent(ctx, id)
	if err != nil {
		return err
	}
	student.GraduationStatus = graduationStatus

	studentJSON, err := json.Marshal(student)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, studentJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Updated graduation status for student: %s", id)
	err = ctx.GetStub().SetEvent("UpdateGraduationStatus", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

//Add a function to create multiple student records in a single transaction.
func (s *StudentContract) CreateStudents(ctx contractapi.TransactionContextInterface, students []*Student) error {
	for _, student := range students {
		err := s.CreateStudent(ctx, student.ID, student.Name, student.DateOfBirth, student.Gender, student.GraduationStatus)
		if err != nil {
			return err
		}
	}
	return nil
}

//Implement functions to query students by specific attributes, such as gender, graduation status, or age range.
func (s *StudentContract) QueryStudentsByGender(ctx contractapi.TransactionContextInterface, gender string) ([]*Student, error) {
	queryString := fmt.Sprintf(`{"selector":{"gender":"%s"}}`, gender)
	return s.queryStudents(ctx, queryString)
}

func (s *StudentContract) QueryStudentsByGraduationStatus(ctx contractapi.TransactionContextInterface, graduationStatus bool) ([]*Student, error) {
	queryString := fmt.Sprintf(`{"selector":{"graduationStatus":%t}}`, graduationStatus)
	return s.queryStudents(ctx, queryString)
}

func (s *StudentContract) QueryStudentsByAgeRange(ctx contractapi.TransactionContextInterface, minAge int, maxAge int) ([]*Student, error) {
	currentDate := time.Now()

	// Calculate the birthdate range
	maxBirthDate := currentDate.AddDate(-minAge, 0, 0)
	minBirthDate := currentDate.AddDate(-maxAge, 0, 0)

	queryString := fmt.Sprintf(`{"selector":{"dateOfBirth":{"$gte":"%s","$lte":"%s"}}}`, minBirthDate.Format(time.RFC3339), maxBirthDate.Format(time.RFC3339))
	return s.queryStudents(ctx, queryString)
}

func (s *StudentContract) queryStudents(ctx contractapi.TransactionContextInterface, queryString string) ([]*Student, error) {
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var students []*Student
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		var student Student
		err = json.Unmarshal(queryResponse.Value, &student)
		if err != nil {
			return nil, err
		}
		students = append(students, &student)
	}
	return students, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(StudentContract))
	if err != nil {
		fmt.Printf("Error create student chaincode: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting student chaincode: %s", err.Error())
	}
}
