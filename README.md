# Student Management System Using Hyperledger Fabric

## Introduction

The Student Management System is a blockchain-based application built using Hyperledger Fabric. This system provides secure, transparent, and efficient management of student records. By leveraging the decentralized nature of blockchain technology, the system ensures data integrity and privacy for sensitive student information.

## Features

- **Secure Data Storage:** Blockchain technology ensures that student records are securely stored and tamper-proof.
- **Transparent Transactions:** All transactions are auditable and transparent, enhancing trust among stakeholders.
- **Decentralized Access Control:** Role-based access control ensures that only authorized users can view or modify student data.
- **Real-time Updates:** Student records are updated in real-time, ensuring consistency across the network.
- **Enhanced Privacy:** Sensitive information is protected through encryption and compliance with data protection regulations.

## Components

- **Frontend:** HTML, CSS, and JavaScript for the user interface.
- **Backend:** Hyperledger Fabric for the blockchain infrastructure and Node.js for server-side logic.
- **Database:** CouchDB as the state database for Hyperledger Fabric.

## Prerequisites

- [Node.js](https://nodejs.org/) (v14.x or higher)
- [Docker](https://www.docker.com/) and Docker Compose
- [Hyperledger Fabric](https://www.hyperledger.org/use/fabric)
- [VS Code]()

## Installation

### Hyperledger Fabric Setup

1. Clone the Hyperledger Fabric samples repository:
   ```sh
   git clone git@gitlab.com:3rd-year-2nd-sem/stddatamgt.git
   cd stddatamgt

## Launch the test network:

    cd wallet-project
    node test-chaincode.js
    node wallet.js

## Application Setup:

### Clone the project repository:
git clone git clone git@gitlab.com:3rd-year-2nd-sem/stddatamgt.git
cd stddatamgt/backend

### Install dependencies:

npm install

## Start the application:

npm start

## Usage
### Creating a New Student
Navigate to the registration page (e.g., http://localhost:3000/students).

Fill in the student details and submit the form.
The student record will be added to the blockchain.

### Updating Student Information
Navigate to the update page (e.g., http://localhost:3000/students).
Enter the student ID and the updated details.
Submit the form to update the student record on the blockchain.
### Batch Create Students
Navigate to the batch creation page (e.g., http://localhost:3000/students).

Add multiple student records by filling out the forms.
Submit the form to add all students to the blockchain at once.
### Viewing Student Records
Navigate to the details page (e.g., http://localhost:3000/students/{id}).

Enter the student ID to view the details of a specific student.


## Contributing

Fork the repository.
Create a new branch (git checkout -b feature-branch).
Make your changes.
Commit your changes (git commit -m 'Add some feature').
Push to the branch (git push origin feature-branch).
Open a pull request.


## Acknowledgements
Hyperledger Fabric
Node.js
Docker
