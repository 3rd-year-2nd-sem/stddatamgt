#!/bin/bash
# Organization specific context and identifiers
export ORG_CONTEXT=university
export ORG_NAME=University
export CORE_PEER_LOCALMSPID=UniversityOrgMSP

# Logging specifications
export FABRIC_LOGGING_SPEC=INFO

# Location of the core.yaml
export FABRIC_CFG_PATH=/workspaces/Student/config/university

# Address of the peer
export CORE_PEER_ADDRESS=uni.std.com:7051

# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/workspaces/Student/config/crypto-config/peerOrganizations/university.com/users/Admin@university.com/msp

# Address of the orderer
export ORDERER_ADDRESS=orderer.std.com:7050

# export ORDERER_CA=/workspaces/Student/config/crypto-config/ordererOrganizations/std.com/ca/ca.std.com-cert.pem
# export PEER0_COLLEGEORG_CA=/workspaces/Student/config/crypto-config/peerOrganizations/college.com/ca/ca.college.com-cert.pem
# export CC_PACKAGE_ID=studentmgt.1.0-1.0:a77f6ed313043c62537552cad65c82afc1492d2d386d978b01eef04ef397cab0


# TLS configuration
export CORE_PEER_TLS_ENABLED=false

#### Chaincode related properties
export CC_NAME="studentmgt"
export CC_PATH="./chaincodes/studentmgt/"
export CC_CHANNEL_ID="studentchannel"
export CC_LANGUAGE="golang"

# Properties of Chaincode
export INTERNAL_DEV_VERSION="1.0"
export CC_VERSION="1.0"
export CC2_PACKAGE_FOLDER="./chaincodes/packages/"
export CC2_SEQUENCE=1
export CC2_INIT_REQUIRED="--init-required"

# Create the package with this name
export CC_PACKAGE_FILE="$CC2_PACKAGE_FOLDER$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION.tar.gz"

# Extracts the package ID for the installed chaincode
export CC_LABEL="$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION"

# List the channels
peer channel list
