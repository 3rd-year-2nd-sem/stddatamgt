PATH="/workspaces/Student/bin:$PATH"

cryptogen generate --config=./crypto-config.yaml --output=crypto-config

configtxgen -outputBlock ./orderer/studentmanagementorderergenesis.block -channelID ordererchannel -profile StudentManagementOrdererGenesis

configtxgen -outputCreateChannelTx ./studentchannel/studentchannel.tx -channelID studentchannel -profile StudentChannel


# create channel
peer channel create -c studentchannel -f ./config/studentchannel/studentchannel.tx --outputBlock ./config/studentchannel/studentchannel.block -o $ORDERER_ADDRESS

# join channel
peer channel join -b ./config/studentchannel/studentchannel.block -o $ORDERER_ADDRESS


. tool-bins/set_college_env.sh 

# Package the chaincodes
peer lifecycle chaincode package $CC_PACKAGE_FILE -p $CC_PATH --label $CC_LABEL

# install the chaincode
peer lifecycle chaincode install $CC_PACKAGE_FILE

peer lifecycle chaincode queryinstalled

CC_PACKAGE_ID=studentmgt.1.0-1.0:a77f6ed313043c62537552cad65c82afc1492d2d386d978b01eef04ef397cab0

peer lifecycle chaincode approveformyorg -n studentmgt -v 1.0 -C studentchannel --sequence 1 --package-id $CC_PACKAGE_ID -o orderer.std.com:7050

peer lifecycle chaincode checkcommitreadiness -n studentmgt -v 1.0 -C studentchannel --sequence 1 -o orderer.std.com:7050


peer lifecycle chaincode commit -n studentmgt -v 1.0 -C studentchannel --sequence 1 -o orderer.std.com:7050

peer lifecycle chaincode querycommitted -n studentmgt -C studentchannel


.... -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"CreateStudent","Args":["1", "Tshering Lham", "2000-01-01T00:00:00Z", "Male", "false"]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"UpdateStudent","Args":["1", "Jane Doe", "2000-01-01T12:00:00Z", "Female", "true"]}' -o orderer.std.com:7050

peer chaincode query -C studentchannel -n studentmgt -c '{"function":"ReadStudent","Args":["1"]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"DeleteStudent","Args":["1"]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"CreateStudents","Args":["[{\"id\":\"7\",\"name\":\"New Student 4\",\"dateOfBirth\":\"2000-01-01T00:00:00Z\",\"gender\":\"Male\",\"graduationStatus\":false},{\"id\":\"8\",\"name\":\"New Student 5\",\"dateOfBirth\":\"2001-02-02T00:00:00Z\",\"gender\":\"Female\",\"graduationStatus\":true},{\"id\":\"9\",\"name\":\"New Student 6\",\"dateOfBirth\":\"2002-03-03T00:00:00Z\",\"gender\":\"Male\",\"graduationStatus\":false}]"]}' -o orderer.std.com:7050

peer chaincode query -C studentchannel -n studentmgt -c '{"function":"GetRecordCount","Args":[]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"QueryStudentsByGender","Args":["Male"]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"QueryStudentsByGraduationStatus","Args":["true"]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"QueryStudentsByAgeRange","Args":["18", "25"]}' -o orderer.std.com:7050

peer chaincode invoke -C studentchannel -n studentmgt -c '{"function":"GetStudentHistory","Args":["10"]}' -o orderer.std.com:7050
