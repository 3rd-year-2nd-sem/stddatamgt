document.getElementById('studentForm').addEventListener('submit', async function (event) {
    event.preventDefault();

    const formData = new FormData(event.target);
    const dateOfBirth = formData.get('dateOfBirth');

    // Convert the dateOfBirth to the required format
    const formattedDateOfBirth = new Date(dateOfBirth).toISOString();

    // Convert graduation status to a boolean
    const graduationStatus = formData.get('graduationStatus') === 'Graduated';

    const data = {
        id: formData.get('id'),
        name: formData.get('name'),
        dateOfBirth: formattedDateOfBirth,
        gender: formData.get('gender'),
        graduationStatus: graduationStatus
    };

    try {
        const response = await fetch('/students', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (response.status === 204) {
            document.getElementById('message').textContent = 'Student successfully registered!';
            document.getElementById('message').style.color = 'green';
        } else {
            const errorText = await response.text();
            document.getElementById('message').textContent = `Failed to register student: ${errorText}`;
        }
    } catch (error) {
        document.getElementById('message').textContent = `Failed to register student: ${error.message}`;
    }
});
