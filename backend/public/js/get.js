// Function to handle form submission for searching student information
document.getElementById('studentSearchForm').addEventListener('submit', async function (event) {
    event.preventDefault();

    const studentId = document.getElementById('studentId').value;
    const studentInfoDiv = document.getElementById('studentInfo');
    const messageDiv = document.getElementById('message');

    try {
        // Fetch student information
        const studentResponse = await fetch(`/students/${studentId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (studentResponse.ok) {
            const student = await studentResponse.json();
            studentInfoDiv.innerHTML = `
                <h2>Student Information</h2>
                <p><strong>ID:</strong> ${student.id}</p>
                <p><strong>Name:</strong> ${student.name}</p>
                <p><strong>Date of Birth:</strong> ${new Date(student.dateOfBirth).toLocaleDateString()}</p>
                <p><strong>Gender:</strong> ${student.gender}</p>
                <p><strong>Graduation Status:</strong> ${student.graduationStatus ? 'Graduated' : 'Not Graduated'}</p>
                <button onclick="deleteStudent('${student.id}')">Delete Student</button>
                <button onclick="getStudentHistory('${student.id}')">View  History</button>
            `;
            messageDiv.textContent = '';
        } else {
            const errorText = await studentResponse.text();
            studentInfoDiv.innerHTML = '';
            messageDiv.textContent = `Failed to retrieve student information: ${errorText}`;
        }
    } catch (error) {
        studentInfoDiv.innerHTML = '';
        messageDiv.textContent = `Failed to retrieve student information: ${error.message}`;
    }
});

// Function to handle deleting a student
async function deleteStudent(studentId) {
    const confirmation = confirm("Are you sure you want to delete this student?");
    if (confirmation) {
        try {
            const response = await fetch(`/students/${studentId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.ok) {
                document.getElementById('message').textContent = 'Student deleted successfully!';
                document.getElementById('studentInfo').innerHTML = '';
            } else {
                const errorText = await response.text();
                document.getElementById('message').textContent = `Failed to delete student: ${errorText}`;
            }
        } catch (error) {
            document.getElementById('message').textContent = `Failed to delete student: ${error.message}`;
        }
    }
}

// Function to handle fetching student transaction history
// Function to handle fetching student transaction history
async function getStudentHistory(studentId) {
    try {
        const response = await fetch(`/students/${studentId}/history`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.ok) {
            const history = await response.json();
            const historyList = history.map(transaction => {
                return `
                    <li>
                        <p><strong>ID:</strong> ${transaction.id}</p>
                        <p><strong>Name:</strong> ${transaction.name}</p>
                        <p><strong>Date of Birth:</strong> ${new Date(transaction.dateOfBirth).toLocaleDateString()}</p>
                        <p><strong>Gender:</strong> ${transaction.gender}</p>
                        <p><strong>Graduation Status:</strong> ${transaction.graduationStatus ? 'Graduated' : 'Not Graduated'}</p>
                    </li>
                `;
            }).join('');
            document.getElementById('message').textContent = '';
            document.getElementById('studentInfo').innerHTML = `
                <h2>History</h2>
                <ul>${historyList}</ul>
            `;
        } else {
            const errorText = await response.text();
            document.getElementById('message').textContent = `Failed to retrieve transaction history: ${errorText}`;
            document.getElementById('studentInfo').innerHTML = '';
        }
    } catch (error) {
        document.getElementById('message').textContent = `Failed to retrieve transaction history: ${error.message}`;
        document.getElementById('studentInfo').innerHTML = '';
    }
}

