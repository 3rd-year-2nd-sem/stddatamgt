document.getElementById('studentUpdateForm').addEventListener('submit', async function (event) {
    event.preventDefault();

    const formData = new FormData(event.target);
    const studentId = formData.get('studentId'); // Get student ID from input field
    
    // Extracting and formatting dateOfBirth
    const dateOfBirthInput = formData.get('dateOfBirth');
    const formattedDateOfBirth = new Date(dateOfBirthInput).toISOString();

    // Convert graduationStatus to boolean
    const graduationStatus = formData.get('graduationStatus') === 'Graduated';

    const data = {
        name: formData.get('name'),
        dateOfBirth: formattedDateOfBirth,
        gender: formData.get('gender'),
        graduationStatus: graduationStatus
    };

    try {
        const response = await fetch(`/students/${studentId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (response.status === 204) {
            document.getElementById('message').textContent = 'Student information updated successfully!';
            document.getElementById('message').style.color = 'green';
        } else {
            const errorText = await response.text();
            document.getElementById('message').textContent = `Failed to update student information: ${errorText}`;
        }
    } catch (error) {
        document.getElementById('message').textContent = `Failed to update student information: ${error.message}`;
    }
});
